package org.pragmaticmodeling.pxdoc.uml.common.lib;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractUmlDelegate implements IUMLDelegate {

	private Set<EStructuralFeature> excludedFeatures = null;
	private Set<EClass> excludedEClasses;

	@Override
	public Set<EClass> getExcludedEClasses() {
		if (excludedEClasses == null) {
			initExcludedEClasses();
		}
		return excludedEClasses;
	}

	private void initExcludedEClasses() {
		excludedEClasses = new HashSet<EClass>();
		addCommonExcludedEClasses();
		addVersionSpecificExcludedEClasses();
	}

	protected void addVersionSpecificExcludedEClasses() {

	}

	protected void addCommonExcludedEClasses() {
		addExcludedEClass(UMLPackage.Literals.ASSOCIATION);
		addExcludedEClass(UMLPackage.Literals.COMMENT);
		addExcludedEClass(UMLPackage.Literals.CONNECTOR);
		addExcludedEClass(UMLPackage.Literals.CONSTRAINT);
		addExcludedEClass(EcorePackage.Literals.EANNOTATION);
		addExcludedEClass(UMLPackage.Literals.INTERFACE_REALIZATION);
		addExcludedEClass(UMLPackage.Literals.OPERATION);
		addExcludedEClass(UMLPackage.Literals.PACKAGE_IMPORT);
		addExcludedEClass(UMLPackage.Literals.PORT);
		addExcludedEClass(UMLPackage.Literals.PRIMITIVE_TYPE);
		addExcludedEClass(UMLPackage.Literals.PROPERTY);
		addExcludedEClass(UMLPackage.Literals.VALUE_SPECIFICATION);
	}
	
	@Override
	public void addExcludedEClass(EClass eClass) {
		excludedEClasses.add(eClass);
	}

	@Override
	public Set<EStructuralFeature> getExcludedFeatures() {
		if (excludedFeatures == null) {
			initExcludedFeatures();
		}
		return excludedFeatures;
	}

	private void initExcludedFeatures() {
		excludedFeatures = new HashSet<EStructuralFeature>();
		addCommonExcludedFeatures();
		addVersionSpecificExcludedFeatures();
	}

	protected abstract void addVersionSpecificExcludedFeatures();

	protected void addCommonExcludedFeatures() {
		addExcludedFeature(UMLPackage.Literals.ACTION__CONTEXT);
		addExcludedFeature(UMLPackage.Literals.ACTIVITY_NODE__ACTIVITY);
		addExcludedFeature(UMLPackage.Literals.CLASSIFIER__FEATURE);
		addExcludedFeature(UMLPackage.Literals.CLASSIFIER__ATTRIBUTE);
		addExcludedFeature(UMLPackage.Literals.DEPENDENCY__CLIENT);
		addExcludedFeature(UMLPackage.Literals.DEPENDENCY__SUPPLIER);
		addExcludedFeature(UMLPackage.Literals.ELEMENT__OWNED_ELEMENT);
		addExcludedFeature(UMLPackage.Literals.ELEMENT__OWNED_COMMENT);
		addExcludedFeature(UMLPackage.Literals.ELEMENT__OWNER);
		addExcludedFeature(UMLPackage.Literals.FEATURE__FEATURING_CLASSIFIER);
		addExcludedFeature(UMLPackage.Literals.NAMED_ELEMENT__NAME);
		addExcludedFeature(UMLPackage.Literals.NAMED_ELEMENT__QUALIFIED_NAME);
		addExcludedFeature(UMLPackage.Literals.NAMED_ELEMENT__NAMESPACE);
		addExcludedFeature(UMLPackage.Literals.NAMED_ELEMENT__VISIBILITY);
		addExcludedFeature(UMLPackage.Literals.NAMESPACE__OWNED_MEMBER);
		addExcludedFeature(UMLPackage.Literals.NAMESPACE__IMPORTED_MEMBER);
		addExcludedFeature(UMLPackage.Literals.NAMESPACE__MEMBER);
		addExcludedFeature(UMLPackage.Literals.OPERATION__INTERFACE);
		addExcludedFeature(UMLPackage.Literals.OPERATION__CLASS);
		addExcludedFeature(UMLPackage.Literals.PACKAGE__NESTED_PACKAGE);
		addExcludedFeature(UMLPackage.Literals.PACKAGE__NESTING_PACKAGE);
		addExcludedFeature(UMLPackage.Literals.PACKAGE__OWNED_TYPE);
		addExcludedFeature(UMLPackage.Literals.PROPERTY__CLASS);
		// addExcludedFeature(UMLPackage.Literals.PROPERTY__INTERFACE); //FIXME
		addExcludedFeature(UMLPackage.Literals.REDEFINABLE_ELEMENT__REDEFINITION_CONTEXT);
		addExcludedFeature(UMLPackage.Literals.REDEFINABLE_ELEMENT__IS_LEAF);
		addExcludedFeature(UMLPackage.Literals.RELATIONSHIP__RELATED_ELEMENT);
		addExcludedFeature(UMLPackage.Literals.STRUCTURED_CLASSIFIER__ROLE);
		addExcludedFeature(UMLPackage.Literals.TYPE__PACKAGE);
	}

	protected void addExcludedFeatures(Set<EStructuralFeature> features) {
		excludedFeatures.addAll(features);
	}

	@Override
	public void addExcludedFeature(EStructuralFeature feature) {
		excludedFeatures.add(feature);
	}
	
	@Override
	public void removeExcludedEClass(EClass eClass) {
		excludedEClasses.remove(eClass);
	}
	
	@Override
	public void removeExcludedFeature(EStructuralFeature feature) {
		excludedFeatures.remove(feature);
	}

}
