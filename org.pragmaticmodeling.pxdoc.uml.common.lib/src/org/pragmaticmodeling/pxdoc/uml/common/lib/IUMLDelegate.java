package org.pragmaticmodeling.pxdoc.uml.common.lib;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.common.util.CacheAdapter;

public interface IUMLDelegate {

	Set<EStructuralFeature> getExcludedFeatures();

	CacheAdapter getCacheAdapter();

	Set<EClass> getExcludedEClasses();
	
	void addExcludedEClass(EClass eClass);
	
	void removeExcludedEClass(EClass eClass);
	
	void addExcludedFeature(EStructuralFeature feature);
	
	void removeExcludedFeature(EStructuralFeature feature);
	
}
