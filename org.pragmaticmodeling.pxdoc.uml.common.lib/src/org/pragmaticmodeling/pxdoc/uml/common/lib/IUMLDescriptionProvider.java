package org.pragmaticmodeling.pxdoc.uml.common.lib;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.common.util.CacheAdapter;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;

public interface IUMLDescriptionProvider extends IDescriptionProvider {
	
	void installUmlDelegate(IUMLDelegate delegate);
	Set<EStructuralFeature> getExcludedFeatures();
	Set<EClass> getExcludedEClasses();
	CacheAdapter getCacheAdapter();

}
