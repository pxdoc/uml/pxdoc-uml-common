package org.pragmaticmodeling.pxdoc.uml.common.lib;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.uml2.common.util.CacheAdapter;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;

public class UMLDescriptionProvider extends DefaultDescriptionProvider implements IUMLDescriptionProvider {

	private IUMLDelegate delegate;

	public UMLDescriptionProvider(IPxDocGenerator generator, IUMLDelegate delegate) {
		super(generator);
		installUmlDelegate(delegate);
	}

	@Override
	public String getDescription(Object object) {
		if (object instanceof Element) {
			Element element = (Element) object;
			String description = "";
			for (Comment c : element.getOwnedComments()) {
				if (c.getBody() != null) {
					description += c.getBody();
					description += "\n";
				}
			}
			if (description.endsWith("\n"))
				description = description.substring(0, description.length() - 1);
			return description;
		}
		return super.getDescription(object);
	}

	@Override
	public void installUmlDelegate(IUMLDelegate delegate) {
		this.delegate = delegate;
	}

	@Override
	public Set<EStructuralFeature> getExcludedFeatures() {
		if (delegate != null) {
			return delegate.getExcludedFeatures();
		}
		return Collections.emptySet();
	}

	@Override
	public CacheAdapter getCacheAdapter() {
		if (delegate != null) {
			return delegate.getCacheAdapter();
		}
		return null;
	}

	@Override
	public Set<EClass> getExcludedEClasses() {
		if (delegate != null) {
			return delegate.getExcludedEClasses();
		}
		return Collections.emptySet();
	}

}
