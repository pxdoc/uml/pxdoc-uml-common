package org.pragmaticmodeling.pxdoc.uml.common.lib;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.uml2.common.util.CacheAdapter;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.Diagram;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.uml.common.lib.IUMLDescriptionProvider;

@SuppressWarnings("all")
public class UMLCommon extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public UMLCommon(final IPxDocGenerator generator) {
    super(generator);
  }
  
  /**
   * Get the role properties
   */
  public List<Property> getRoleProperties(final Classifier pClassifier) {
    List<Property> lRoles = new ArrayList<Property>();
    EList<Property> _attributes = pClassifier.getAttributes();
    for (final Property lProperty : _attributes) {
      if (((lProperty.getType() != null) && (!(lProperty.getType() instanceof DataType)))) {
        lRoles.add(lProperty);
      }
    }
    EList<Association> _associations = pClassifier.getAssociations();
    for (final Association asso : _associations) {
      EList<Property> _ownedEnds = asso.getOwnedEnds();
      for (final Property role : _ownedEnds) {
        boolean _isNavigable = role.isNavigable();
        if (_isNavigable) {
          Type _type = role.getType();
          boolean _tripleEquals = (_type == pClassifier);
          if (_tripleEquals) {
            if (((role.getOpposite() != null) && (role.getOpposite().getType() == pClassifier))) {
              lRoles.add(role);
            }
          } else {
            lRoles.add(role);
          }
        }
      }
    }
    return lRoles;
  }
  
  /**
   * Get the attribute properties
   */
  public List<Property> getAttributeProperties(final Classifier pClassifier) {
    List<Property> attributes = new ArrayList<Property>();
    EList<Property> _attributes = pClassifier.getAttributes();
    for (final Property prop : _attributes) {
      Type _type = prop.getType();
      boolean _tripleEquals = (_type == null);
      if (_tripleEquals) {
        attributes.add(prop);
      } else {
        Type _type_1 = prop.getType();
        if ((_type_1 instanceof DataType)) {
          attributes.add(prop);
        }
      }
    }
    return attributes;
  }
  
  /**
   * Get the upper bound of the element multiplicity
   */
  public String getUpper(final MultiplicityElement pElement) {
    String upper = "1";
    ValueSpecification vs = pElement.getUpperValue();
    if ((vs != null)) {
      if ((vs instanceof OpaqueExpression)) {
        upper = IterableExtensions.<String>head(((OpaqueExpression)vs).getBodies()).trim();
      } else {
        if ((vs instanceof LiteralInteger)) {
          int _value = ((LiteralInteger)vs).getValue();
          String _plus = ("" + Integer.valueOf(_value));
          upper = _plus;
        } else {
          if ((vs instanceof LiteralUnlimitedNatural)) {
            String _xifexpression = null;
            int _value_1 = ((LiteralUnlimitedNatural)vs).getValue();
            boolean _equals = (_value_1 == (-1));
            if (_equals) {
              _xifexpression = "*";
            } else {
              int _value_2 = ((LiteralUnlimitedNatural)vs).getValue();
              _xifexpression = ("" + Integer.valueOf(_value_2));
            }
            upper = _xifexpression;
          }
        }
      }
    } else {
      String _xifexpression_1 = null;
      int _upper = pElement.getUpper();
      boolean _equals_1 = (_upper == (-1));
      if (_equals_1) {
        _xifexpression_1 = "*";
      } else {
        int _upper_1 = pElement.getUpper();
        _xifexpression_1 = ("" + Integer.valueOf(_upper_1));
      }
      upper = _xifexpression_1;
    }
    return upper;
  }
  
  /**
   * Get lower bound of the element multiplicity
   */
  public String getLower(final MultiplicityElement element) {
    String lower = "0";
    ValueSpecification vs = element.getLowerValue();
    if ((vs != null)) {
      if ((vs instanceof OpaqueExpression)) {
        lower = IterableExtensions.<String>head(((OpaqueExpression)vs).getBodies());
      } else {
        if ((vs instanceof LiteralInteger)) {
          int _value = ((LiteralInteger)vs).getValue();
          String _plus = ("" + Integer.valueOf(_value));
          lower = _plus;
        }
      }
    } else {
      int _lower = element.getLower();
      String _plus_1 = ("" + Integer.valueOf(_lower));
      lower = _plus_1;
    }
    return lower;
  }
  
  /**
   * Returns element multiplicity
   */
  public String multiplicity(final MultiplicityElement me) {
    String multiplicity = "";
    String lLower = this.getLower(me);
    String lUpper = this.getUpper(me);
    if ((lLower.equals("1") && lUpper.equals("1"))) {
      return "1";
    }
    multiplicity = ((lLower + "..") + lUpper);
    return multiplicity;
  }
  
  /**
   * Get the constraints of an element
   */
  public List<Constraint> getConstraints(final EObject element) {
    List<Constraint> constraints = new ArrayList<Constraint>();
    CacheAdapter cacheAdapter = this.getUmlDescriptionProvider().getCacheAdapter();
    Collection<EStructuralFeature.Setting> candidates = cacheAdapter.getNonNavigableInverseReferences(element);
    for (final EStructuralFeature.Setting c : candidates) {
      {
        EObject eObject = c.getEObject();
        EStructuralFeature eFeature = c.getEStructuralFeature();
        if (((eObject instanceof Constraint) && Objects.equal(eFeature, UMLPackage.Literals.CONSTRAINT__CONSTRAINED_ELEMENT))) {
          constraints.add(((Constraint) eObject));
        }
      }
    }
    return constraints;
  }
  
  /**
   * Filter UML Features and Classes that we do not want to include or detail in the documentation
   */
  public void filterUmlFeatures() {
    Set<EStructuralFeature> _excludedFeatures = this.getExcludedFeatures();
    Set<EStructuralFeature> _excludedFeatures_1 = this.getUmlDescriptionProvider().getExcludedFeatures();
    Iterables.<EStructuralFeature>addAll(_excludedFeatures, _excludedFeatures_1);
    Set<EClass> _excludedEClasses = this.getExcludedEClasses();
    Set<EClass> _excludedEClasses_1 = this.getUmlDescriptionProvider().getExcludedEClasses();
    Iterables.<EClass>addAll(_excludedEClasses, _excludedEClasses_1);
  }
  
  /**
   * Set the Description provider, used by the Common Services
   */
  public void setUmlDescriptionProvider(final IUMLDescriptionProvider provider) {
    this.setDescriptionProvider(provider);
  }
  
  /**
   * Get the Description provider, used by the Common Services
   */
  public IUMLDescriptionProvider getUmlDescriptionProvider() {
    IDescriptionProvider _descriptionProvider = this.getDescriptionProvider();
    if ((_descriptionProvider instanceof IUMLDescriptionProvider)) {
      IDescriptionProvider _descriptionProvider_1 = this.getDescriptionProvider();
      return ((IUMLDescriptionProvider) _descriptionProvider_1);
    }
    return null;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Documentation of components
   * FIXME Class + ports/connectors
   */
  public void component(final ContainerElement parent, final Component element, final int level) throws PxDocGenerationException {
    _EmfServices.basicDescription(parent, element, level);
    List<Property> attributes = this.getAttributeProperties(element);
    List<Property> roles = this.getRoleProperties(element);
    List<Operation> operations = element.getOwnedOperations();
    final Function1<Classifier, String> _function = (Classifier it) -> {
      return it.getName();
    };
    List<Classifier> superTypes = IterableExtensions.<Classifier, String>sortBy(Iterables.<Classifier>filter(element.getGenerals(), Classifier.class), _function);
    boolean _isEmpty = superTypes.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj0 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj0, "Super types");
      Iterator<org.eclipse.uml2.uml.Classifier> lObj1 = getIterator(superTypes);
      while (lObj1.hasNext()) {
        _EmfServices.bulletedItem(parent, lObj1.next());
        
      }
    }
    boolean _isEmpty_1 = attributes.isEmpty();
    boolean _not_1 = (!_isEmpty_1);
    if (_not_1) {
      StyleApplication lObj2 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj2, "Attributes");
      Iterator<org.eclipse.uml2.uml.Property> lObj3 = getIterator(attributes);
      while (lObj3.hasNext()) {
        property(parent, lObj3.next());
        
      }
    }
    boolean _isEmpty_2 = roles.isEmpty();
    boolean _not_2 = (!_isEmpty_2);
    if (_not_2) {
      StyleApplication lObj4 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj4, "Relationships");
      Iterator<org.eclipse.uml2.uml.Property> lObj5 = getIterator(roles);
      while (lObj5.hasNext()) {
        property(parent, lObj5.next());
        
      }
    }
    boolean _isEmpty_3 = operations.isEmpty();
    boolean _not_3 = (!_isEmpty_3);
    if (_not_3) {
      StyleApplication lObj6 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj6, "Operations");
      Iterator<org.eclipse.uml2.uml.Operation> lObj7 = getIterator(operations);
      while (lObj7.hasNext()) {
        operation(parent, lObj7.next());
        
      }
    }
    constraints(parent, element);
    Iterator<org.eclipse.emf.ecore.EObject> lObj8 = getIterator(this.getChildren(element));
    while (lObj8.hasNext()) {
      description(parent, lObj8.next(), (level + 1));
      
    }
  }
  
  /**
   * Documentation of properties
   */
  public void property(final ContainerElement parent, final Property attribute) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
    {
      createText(lObj0, attribute.getName());
      createText(lObj0, " : ");
      if (((attribute.getType() != null) && this.hasBookmark(attribute.getType()))) {
        Hyperlink lObj1 = createHyperlink(lObj0, getBookmarkString(this.validBookmark(attribute.getType())), null, null);
        createText(lObj1, attribute.getType().getName());
      } else {
        Type _type = attribute.getType();
        boolean _tripleEquals = (_type == null);
        if (_tripleEquals) {
          createText(lObj0, "null");
        } else {
          createText(lObj0, attribute.getType().getName());
        }
      }
      createText(lObj0, " [");
      createText(lObj0, this.multiplicity(attribute));
      createText(lObj0, "]");
    }
    _CommonServices.smartDocumentation(parent, attribute);
    constraints(parent, attribute);
  }
  
  /**
   * Documentation of parameters
   */
  public void parameter(final ContainerElement parent, final Parameter param) throws PxDocGenerationException {
    if (((param.getType() != null) && this.hasBookmark(param.getType()))) {
      Hyperlink lObj0 = createHyperlink(parent, getBookmarkString(this.validBookmark(param.getType())), null, null);
      createText(lObj0, param.getType().getName());
    } else {
      Type _type = param.getType();
      boolean _tripleEquals = (_type == null);
      if (_tripleEquals) {
        createText(parent, "void");
      } else {
        createText(parent, param.getType().getName());
      }
    }
    createText(parent, " ");
    createText(parent, param.getName());
  }
  
  /**
   * Documentation of operations
   */
  public void operation(final ContainerElement parent, final Operation operation) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
    {
      createImage(lObj0, null, this.getImage(operation), null, null, null, null);
      createText(lObj0, " ");
      if (((operation.getType() != null) && this.hasBookmark(operation.getType()))) {
        Hyperlink lObj1 = createHyperlink(lObj0, getBookmarkString(this.validBookmark(operation.getType())), null, null);
        createText(lObj1, operation.getType().getName());
      } else {
        Type _type = operation.getType();
        boolean _tripleEquals = (_type == null);
        if (_tripleEquals) {
          createText(lObj0, "void");
        } else {
          createText(lObj0, operation.getType().getName());
        }
      }
      createText(lObj0, " ");
      createText(lObj0, operation.getName());
      createText(lObj0, "(");
      Iterator<org.eclipse.uml2.uml.Parameter> lObj2 = getIterator(operation.getOwnedParameters());
      while (lObj2.hasNext()) {
        parameter(lObj0, lObj2.next());
        if (lObj2.hasNext()) {
          createText(lObj0, ", ");
        }
      }
      createText(lObj0, ")");
    }
    _CommonServices.smartDocumentation(parent, operation);
    constraints(parent, operation);
  }
  
  /**
   * Documentation of classes
   * FIXME general + attributes/operations + interfaceRealizations
   */
  public void classDoc(final ContainerElement parent, final org.eclipse.uml2.uml.Class element, final int level) throws PxDocGenerationException {
    _EmfServices.basicDescription(parent, element, level);
    List<Property> attributes = this.getAttributeProperties(element);
    List<Property> roles = this.getRoleProperties(element);
    List<Operation> operations = element.getOwnedOperations();
    final Function1<Classifier, String> _function = (Classifier it) -> {
      return it.getName();
    };
    List<Classifier> superTypes = IterableExtensions.<Classifier, String>sortBy(Iterables.<Classifier>filter(element.getGenerals(), Classifier.class), _function);
    boolean _isEmpty = superTypes.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj0 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj0, "Super types");
      Iterator<org.eclipse.uml2.uml.Classifier> lObj1 = getIterator(superTypes);
      while (lObj1.hasNext()) {
        _EmfServices.bulletedItem(parent, lObj1.next());
        
      }
    }
    boolean _isEmpty_1 = attributes.isEmpty();
    boolean _not_1 = (!_isEmpty_1);
    if (_not_1) {
      StyleApplication lObj2 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj2, "Attributes");
      Iterator<org.eclipse.uml2.uml.Property> lObj3 = getIterator(attributes);
      while (lObj3.hasNext()) {
        property(parent, lObj3.next());
        
      }
    }
    boolean _isEmpty_2 = roles.isEmpty();
    boolean _not_2 = (!_isEmpty_2);
    if (_not_2) {
      StyleApplication lObj4 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj4, "Relationships");
      Iterator<org.eclipse.uml2.uml.Property> lObj5 = getIterator(roles);
      while (lObj5.hasNext()) {
        property(parent, lObj5.next());
        
      }
    }
    boolean _isEmpty_3 = operations.isEmpty();
    boolean _not_3 = (!_isEmpty_3);
    if (_not_3) {
      StyleApplication lObj6 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj6, "Operations");
      Iterator<org.eclipse.uml2.uml.Operation> lObj7 = getIterator(operations);
      while (lObj7.hasNext()) {
        operation(parent, lObj7.next());
        
      }
    }
    constraints(parent, element);
    Iterator<org.eclipse.emf.ecore.EObject> lObj8 = getIterator(this.getChildren(element));
    while (lObj8.hasNext()) {
      description(parent, lObj8.next(), (level + 1));
      
    }
  }
  
  /**
   * Documentation of interfaces
   * general + attributes/operations
   */
  public void interfaceDoc(final ContainerElement parent, final Interface element, final int level) throws PxDocGenerationException {
    _EmfServices.basicDescription(parent, element, level);
    List<Operation> operations = element.getOwnedOperations();
    final Function1<Classifier, String> _function = (Classifier it) -> {
      return it.getName();
    };
    List<Classifier> superTypes = IterableExtensions.<Classifier, String>sortBy(Iterables.<Classifier>filter(element.getGenerals(), Classifier.class), _function);
    boolean _isEmpty = superTypes.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj0 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj0, "Super interfaces");
      Iterator<org.eclipse.uml2.uml.Classifier> lObj1 = getIterator(superTypes);
      while (lObj1.hasNext()) {
        _EmfServices.bulletedItem(parent, lObj1.next());
        
      }
    }
    constraints(parent, element);
    boolean _isEmpty_1 = operations.isEmpty();
    boolean _not_1 = (!_isEmpty_1);
    if (_not_1) {
      StyleApplication lObj2 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj2, "Operations");
      Iterator<org.eclipse.uml2.uml.Operation> lObj3 = getIterator(operations);
      while (lObj3.hasNext()) {
        operation(parent, lObj3.next());
        
      }
    }
  }
  
  /**
   * Documentation of the applying constraints
   */
  public void constraint(final ContainerElement parent, final Constraint constraint) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BulletList"), null, null, null);
    {
      _EmfServices.iconAndText(lObj0, constraint);
      createText(lObj0, " [");
      createText(lObj0, constraint.getSpecification().stringValue());
      createText(lObj0, "]");
    }
  }
  
  /**
   * Add "Applying constraint" part, if relevant
   */
  public void constraints(final ContainerElement parent, final EObject element) throws PxDocGenerationException {
    List<Constraint> constraints = this.getConstraints(element);
    boolean _isEmpty = constraints.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StyleApplication lObj0 = createStyleApplication(parent, true, getBindedStyle("SubTitle"), null, null, null);
      createText(lObj0, "Applying Constraints");
      Iterator<org.eclipse.uml2.uml.Constraint> lObj1 = getIterator(constraints);
      while (lObj1.hasNext()) {
        constraint(parent, lObj1.next());
        
      }
    }
  }
  
  /**
   * Demonstrates how to mix/reuse various way of presenting elements:
   * Each element type is included according to a specific template
   */
  public void description(final ContainerElement parent, final EObject element, final int level) throws PxDocGenerationException {
    boolean _matched = false;
    if (element instanceof Interface) {
      _matched=true;
      interfaceDoc(parent, ((Interface)element), level);
    }
    if (!_matched) {
      if (element instanceof Component) {
        _matched=true;
        component(parent, ((Component)element), level);
      }
    }
    if (!_matched) {
      if (element instanceof org.eclipse.uml2.uml.Class) {
        _matched=true;
        classDoc(parent, ((org.eclipse.uml2.uml.Class)element), level);
      }
    }
    if (!_matched) {
      {
        boolean _isExcluded = this.isExcluded(element);
        if (_isExcluded) {
          return;
        }
        List<Diagram> diagrams = this.getDiagramProvider().createDiagramsQuery(element).notEmpty().execute();
        List<EStructuralFeature> attributes = this.getProperties(element);
        List<EObject> childs = this.getChildren(element);
        if (((level > 0) && (level < 10))) {
          HeadingN lObj0 = createHeadingN(parent, level, true);
          boolean _hasBookmark = this.hasBookmark(element);
          if (_hasBookmark) {
            String lObj1 = this.validBookmark(element);
            Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
            _EmfServices.iconAndText(lObj2, element);
          } else {
            _EmfServices.iconAndText(lObj0, element);
          }
        }
        _CommonServices.smartDocumentation(parent, element);
        boolean _isEmpty = diagrams.isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          Iterator<org.pragmaticmodeling.pxdoc.diagrams.Diagram> lObj3 = getIterator(diagrams);
          while (lObj3.hasNext()) {
            _DiagramServices.includeDiagram(parent, lObj3.next());
            
          }
        }
        constraints(parent, element);
        Iterator<org.eclipse.emf.ecore.EStructuralFeature> lObj4 = getIterator(attributes);
        while (lObj4.hasNext()) {
          _EmfServices.propertyFormDescription(parent, lObj4.next(), element);
          
        }
        Iterator<org.eclipse.emf.ecore.EObject> lObj5 = getIterator(childs);
        while (lObj5.hasNext()) {
          description(parent, lObj5.next(), (level + 1));
          
        }
      }
    }
  }
}
